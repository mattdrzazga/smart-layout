import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.0
import "qrc:/adaptive-layout/src/adaptive-layout" as  AL

Item {
    id: root
    anchors.fill: parent

    property Window rootWindow: null


    function authenticateUser(login, password) {
        if (login === "admin" && password === "admin") {
            return true
        }
        else {
            return false
        }
    }

    Timer {
        id: timer
        running: false
        repeat: false
        interval: 3000
        onTriggered: {
            appLoader.active = true
            loginScreen.close()
            rootWindow.show()
        }
    }

    AL.LoginScreen {
        id: loginScreen
        anchors.fill: parent
        rootWindow: root.rootWindow
        backgroundColor: SMA.isMobile? "#333c4d" : "#53af8b"
        backgroundImage: "qrc:/icons/icons/coloured_pencils-wallpaper-1920x1080.jpg"
        z: 2

        onLoginClicked: {
            Qt.inputMethod.hide()
            if (authenticateUser(login, password)) {
                timer.start()
            }
            else {
                wrongPassword()
            }
        }
    }

    Loader {
        id: appLoader
        active: false
        anchors.fill: parent
        sourceComponent: Rectangle {
            color: "#005043"

            Label {
                anchors.centerIn: parent
                text: "Your application"
                font.pointSize: 20
                color: "white"
            }
        }
    }
}
