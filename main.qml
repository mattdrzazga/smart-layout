import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import "qrc:/adaptive-layout/src/adaptive-layout" as  AL
import "qrc:/themed-layout/src/themed-layout" as  SL

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    id: window


//    SL.ExampleView {
//        width: 100
//        height: 100
//    }

//    ListViewPreview { }

    toolBar: ToolBarPreview {
        onMenuButtonClicked: sideMenuPreview.sideMenu.toggle()
    }


    SideMenuPreview {
        id: sideMenuPreview
    }


//    LoginScreenPreview {
//        anchors.fill: parent
//        rootWindow: window
//    }

//    TabViewPreview {
//        anchors.fill: parent
//    }

//    ColorPickerPreview { }


//    ButtonPreview {
//        anchors.fill: parent
//    }
}
