import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import "qrc:/adaptive-layout/src/adaptive-layout" as  AL

AL.ToolBar {
    color: "#03a9f4"

    signal menuButtonClicked()

    toolBarLayout: RowLayout {
        anchors.fill: parent

        ToolButton {
            iconSource: "qrc:/icons/icons/ic_menu_black_48dp.png"
            onClicked: menuButtonClicked()
        }
        ToolButton {
            iconSource: "qrc:/icons/icons/ic_refresh_black_48dp.png"
        }
        ToolButton {
            iconSource: "qrc:/icons/icons/ic_search_black_48dp.png"
        }
        Item { Layout.fillWidth: true }

        Label {
            text: SMA.isMobile? "Mobile" : "Desktop"
        }
    }
}
