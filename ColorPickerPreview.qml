import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import "qrc:/themed-layout/src/themed-layout" as TL
import "qrc:/themed-layout/themes/android/src/themed-layout/themes/android/" as TLA
import "qrc:/themed-layout/themes/desktop/src/themed-layout/themes/desktop" as TLD

Rectangle {
    id: root
    anchors.fill: parent
    color: "lightgrey"

    property var colors: ["#807573", "#f4dfdc", "#52b2b5", "#842541", "#d93c6a", "#776153", "#fbccae", "#494632"]

    TL.ColorPicker {
        id: colorPicker
        model: colors
        style: TLA.ColorPickerStyle {}
        onColorSelected: {
            root.color = color
            colorPicker.close()
        }
    }

    Button {
        anchors.centerIn: parent
        text: "Select Color"
        onClicked: colorPicker.open()
    }
}
