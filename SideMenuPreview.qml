import QtQuick 2.5
import "qrc:/adaptive-layout/src/adaptive-layout" as  AL
import QtQuick.Controls 1.4

Item {
    anchors.fill: parent

    property alias sideMenu: sideMenu

    AL.SideMenu {
        id: sideMenu
        width: SMA.isMobile? parent.width * 0.8 : SMA.scaled(100)
        height: parent.height

        ExclusiveGroup { id: group }

        actions: [
            Action { text: "Search";    iconSource: "qrc:/icons/icons/ic_search_black_48dp.png";    checkable: true; exclusiveGroup: group; onTriggered: label.text = text + " option "; checked: true },
            Action { text: "Done";      iconSource: "qrc:/icons/icons/ic_done_black_48dp.png";      checkable: true; exclusiveGroup: group; onTriggered: label.text = text + " option " },
            Action { text: "Deleted";   iconSource: "qrc:/icons/icons/ic_clear_black_48dp.png";    checkable: true; exclusiveGroup: group; onTriggered: label.text = text + " option " }
        ]
    }

    Rectangle {
        color: "#50c878"
        anchors.left: SMA.isMobile? parent.left : sideMenu.right
        height: parent.height
        anchors.right: parent.right

        Button {
            text: "Pointless Button"
        }

        Label {
            id: label
            anchors.centerIn: parent
        }
    }
}
