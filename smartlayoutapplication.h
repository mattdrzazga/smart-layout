#ifndef SMARTLAYOUTAPPLICATION_H
#define SMARTLAYOUTAPPLICATION_H

#include <QObject>

class SmartLayoutApplication : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isMobile READ isMobile WRITE setIsMobile NOTIFY isMobileChanged)
    Q_PROPERTY(qreal ratio READ ratio CONSTANT)

public:
    explicit SmartLayoutApplication(QObject *parent = 0);

    inline bool isMobile() const { return mIsMobile; }
    void setIsMobile(bool isMobile);

    inline qreal ratio() const {return mRatio; }

    Q_INVOKABLE inline int scaled(int px) { return mRatio * px; }

signals:
    void isMobileChanged();

private:
    bool mIsMobile = false;
    qreal mRatio;
};

#endif // SMARTLAYOUTAPPLICATION_H
