import QtQuick 2.5
import QtQuick.Layouts 1.1
import "qrc:/themed-layout/src/themed-layout" as TL
import "qrc:/themed-layout/themes/desktop/src/themed-layout/themes/desktop" as TLD
import "qrc:/themed-layout/themes/android/src/themed-layout/themes/android" as TLA

Rectangle {
    id: root

    property Component desktopButtonStyle: TLD.ButtonStyle { }
    property Component mobileButtonStyle: TLA.ButtonStyle { }


    ColumnLayout {
        anchors.fill: parent

        TL.Button {
            Layout.preferredWidth: SMA.scaled(200)
            Layout.preferredHeight: SMA.scaled(120)
            Layout.alignment: Qt.AlignHCenter
            text: "Click Me"
            color: "#86f4a4"
            style: SMA.isMobile? root.mobileButtonStyle : root.desktopButtonStyle
            onClicked: console.log("clicked1")
        }

        TL.Button {
            Layout.preferredWidth: SMA.scaled(200)
            Layout.preferredHeight: SMA.scaled(120)
            Layout.alignment: Qt.AlignHCenter
            text: "Click Me"
            color: "white"
            style: SMA.isMobile? root.mobileButtonStyle : root.desktopButtonStyle
            onClicked: console.log("clicked2")
        }

        TL.Button {
            Layout.preferredWidth: SMA.scaled(200)
            Layout.preferredHeight: SMA.scaled(120)
            Layout.alignment: Qt.AlignHCenter
            text: "Click Me"
            color: "#c4df9b"
            style: SMA.isMobile? root.mobileButtonStyle : root.desktopButtonStyle
            onClicked: console.log("clicked3")
        }

        TL.Button {
            Layout.preferredWidth: SMA.scaled(200)
            Layout.preferredHeight: SMA.scaled(200)
            Layout.alignment: Qt.AlignHCenter
            text: "Click Me"
            color: "#57aefc"
            style: SMA.isMobile? root.mobileButtonStyle : root.desktopButtonStyle
            onClicked: console.log("clicked4")
        }
    }
}
