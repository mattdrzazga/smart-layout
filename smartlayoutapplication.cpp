#include "smartlayoutapplication.h"
#include <QApplication>
#include <QRect>
#include <QScreen>

SmartLayoutApplication::SmartLayoutApplication(QObject *parent) : QObject(parent)
{
    QString platformName = qApp->platformName();
    if (platformName == "android") {
        mIsMobile = true;
    }
    else if (platformName == "windows") {
        mIsMobile = false;
    }
    else if (platformName == "xcb") {
        mIsMobile = false;
    }

    QRect rect = qApp->primaryScreen()->geometry();
    mRatio = mIsMobile ? qMin(qMax(rect.width(), rect.height())/1136. , qMin(rect.width(), rect.height())/640.) : 1;
}

void SmartLayoutApplication::setIsMobile(bool isMobile)
{
    mIsMobile = isMobile;
    emit isMobileChanged();
}
