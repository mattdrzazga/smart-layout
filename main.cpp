#include <QApplication>
#include <QQmlApplicationEngine>
#include "smartlayoutapplication.h"
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    SmartLayoutApplication smartLayoutApplication;

    QQmlApplicationEngine engine;
    //We make SmartLayoutApplication object visible from QML scope.
    engine.rootContext()->setContextProperty("SMA", &smartLayoutApplication);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
