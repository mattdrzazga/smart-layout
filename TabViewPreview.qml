import QtQuick 2.5
import QtQuick.Controls 1.4
import "qrc:/adaptive-layout/src/adaptive-layout" as  AL

Rectangle {

    AL.TabView {
        anchors.fill: parent

        tabs: [
            Tab {
                title: "Blue"
                Rectangle { color: "#0057e7" }
            },
            Tab {
                title: "Orange"
                Rectangle { color: "#ffa700" }
            },
            Tab {
                title: "Green"
                Rectangle { color: "#0aa05c" }
            }
        ]
    }
}
