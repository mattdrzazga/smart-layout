import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import "qrc:/adaptive-layout/src/adaptive-layout" as  AL

AL.ListView {
    id: lv
    anchors.fill: parent
    model: colors
    focus: true

    property var colors: [ "aqua", "blueviolet", "limegreen", "mediumaquamarine", "midnightblue", "darkkhaki", "olivedrab", "palegreen", "deepskyblue", "seagreen" ]

    listDelegate: Item {
        // THIS IS BRILLIANT
        // THESE PROPERTIES MUST EXIST IN A DELEGATE
        property var model
        property var modelData

        height: SMA.isMobile? SMA.scaled(160) : SMA.scaled(60)

        Label {
            id: label
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: SMA.isMobile? SMA.scaled(30) : SMA.scaled(10)
            text: "color[" + model.index + "] = " + modelData
        }

        Rectangle {
            id: rectColor
            width: SMA.isMobile? SMA.scaled(70) : SMA.scaled(30)
            height: SMA.isMobile? SMA.scaled(70) : SMA.scaled(30)
            anchors.right: parent.right
            anchors.rightMargin: SMA.isMobile? SMA.scaled(30) : SMA.scaled(10)
            anchors.verticalCenter: parent.verticalCenter
            color: modelData
        }

        DropShadow {
            anchors.fill: rectColor
            radius: 12
            samples: 25
            spread: 0
            color: "#80000000"
            transparentBorder: true
            source: rectColor
            cached: true
            smooth: true
        }
    }

    detailComponent: Rectangle {
        id: detailRect
        color: lv.listView.currentIndex != -1? colors[lv.listView.currentIndex] : "white"
    }

    Connections {
        target: lv.listView
        onCurrentIndexChanged: {
            console.log("currentIndex changed " + lv.listView.currentIndex)
        }
    }
}
