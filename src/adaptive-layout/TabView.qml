import QtQuick 2.5
import QtQuick.Controls 1.4
import "qrc:/adaptive-layout/android/src/adaptive-layout/android" as Android
import "qrc:/adaptive-layout/desktop/src/adaptive-layout/desktop" as Desktop

Loader {
    id: root

    property list<Tab> tabs

    property Component androidTabView: Android.TabViewComponent {
        tabs: root.tabs
    }

    property Component desktopTabView: Desktop.TabViewComponent {
        tabs: root.tabs
    }

    sourceComponent: SMA.isMobile? androidTabView : desktopTabView
}
