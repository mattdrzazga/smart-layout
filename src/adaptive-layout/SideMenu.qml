import QtQuick 2.5
import QtQuick.Controls 1.4
import "qrc:/adaptive-layout/android/src/adaptive-layout/android" as Android
import "qrc:/adaptive-layout/desktop/src/adaptive-layout/desktop" as Desktop

Loader {
    id: root
    z: 1
    x: SMA.isMobile? 0 : -width

    //  Actions used to populate menu.
    property list<Action> actions

    //  Mobile version of the SideMenu, using MobileMenuTile as basic menu item.
    property Component androidSideMenu: Android.SideMenuComponent {
        actions: root.actions
    }

    //  Desktop version of the SideMenu, using DesktopMenuTile as basic menu item.
    property Component desktopSideMenu: Desktop.SideMenuComponent {
        actions: root.actions
    }

    //  Function that toggles visibility of the menu.
    function toggle() {
        if (root.item) {
            if (root.item.menuVisible) {
                if ( !SMA.isMobile ) root.x = -root.width
                root.item.hide()
            }
            else {
                if ( !SMA.isMobile ) root.x = 0
                root.item.show()
            }
        }
    }

    sourceComponent: SMA.isMobile? androidSideMenu : desktopSideMenu
}
