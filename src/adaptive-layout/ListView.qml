import QtQuick 2.5
import "qrc:/adaptive-layout/android/src/adaptive-layout/android" as Android
import "qrc:/adaptive-layout/desktop/src/adaptive-layout/desktop" as Desktop

Loader {
    id: root
    asynchronous: true

    /* This property holds the model providing data for the list. */
    property var model: undefined

    /* The delegate provides a template defining each item instantiated by the list. */
    property Component listDelegate: undefined

    /* This is component that is detailed view of a selected item. */
    property Component detailComponent: undefined

    /* This property holds the listView value of created ListViewComponent. */
    property ListView listView: null

    /* This component is for mobile devices. */
    property Component androidListView: Android.ListViewComponent {
        model: root.model? root.model : undefined
        listDelegate: root.listDelegate? root.listDelegate : undefined
        detailComponent: root.detailComponent? root.detailComponent : undefined

        Binding { target: root; property: "listView"; value: listView }
    }

    /* This component is for desktop devices. */
    property Component desktopListView: Desktop.ListViewComponent {
        model: root.model? root.model : undefined
        listDelegate: root.listDelegate? root.listDelegate : undefined
        detailComponent: root.detailComponent? root.detailComponent : undefined

        Binding { target: root; property: "listView"; value: listView }
    }

    /* If running on mobile, load mobile UI, otherwise load desktop UI. */
    sourceComponent: SMA.isMobile? androidListView : desktopListView
}
