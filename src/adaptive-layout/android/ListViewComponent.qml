import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import "qrc:/core/src/core"

Rectangle {
    id: root
    color:  Qt.darker("white", 1.2)

    property var model: undefined
    property Component listDelegate: undefined
    property Component detailComponent: undefined
    property string cellColor: "white"
    property string highlightColor: "#18aa42"
    property ListView listView: undefined

    property Component listViewComponent: ListView {
        id: listView
        model: root.model
        boundsBehavior: Flickable.DragOverBounds

        // Entire cell, with frame and shadows and etc.
        delegate: Item {
            id: rootItem
            width: parent.width

            // Actual visible rectangle.
            Rectangle {
                id: rect
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.right: parent.right
                height: loader.item.height

                color: "white"

                Loader {
                    id: loader
                    sourceComponent: root.listDelegate
                    width: parent.width
                    onLoaded: {
                        item.model = model
                        item.modelData = modelData

                        rootItem.height = item.height
                    }

                    Connections {
                        target: loader.item
                        onHeightChanged: rootItem.height = item? item.height : 0
                    }

                    RippleEffect { source: root }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            stackView.push({ item: detailComponent, properties: {color: modelData} })
                        }
                    }
                }
            }

            Rectangle {
                width: parent.width
                height: 1
                color: "#cccccc"
                anchors.bottom: parent.bottom
            }
        }

        Component.onCompleted: root.listView = listView
    }



    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: listViewComponent
        focus: true

        Keys.onReleased: {
            if (event.key === Qt.Key_Back || (event.key === Qt.Key_Left && (event.modifiers & Qt.AltModifier))) {
                if (stackView.depth > 1) {
                    event.accepted = true
                    pop(detailComponent)
                }
            }
        }
    }
}
