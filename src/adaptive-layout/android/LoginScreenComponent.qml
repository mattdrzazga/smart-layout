import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    signal loginClicked(string login, string password)
    signal signUpClicked()
    signal passwordForgotClicked()

    property bool withBlur: true
    property string backgroundImage


    function wrongPassword() {
        invalidDataLabel.visible = true
        busyIndicator.running = false
    }


    function close() {
        root.opacity = 0
    }


    Behavior on opacity { PropertyAnimation {} }


    // http://wallpaperswide.com/coloured_pencils-wallpapers.html
    Image {
        id: image
        anchors.fill: parent
        source: backgroundImage
    }

    FastBlur {
        anchors.fill: image
        source: withBlur && image.status == Image.Ready ? image : undefined
        radius: 64
    }


    ColumnLayout {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 30
        anchors.rightMargin: 30
        anchors.verticalCenter: parent.verticalCenter
        height: implicitHeight

        TextField {
            id: loginTextField
            Layout.fillWidth: true
            Layout.maximumWidth: SMA.scaled(600)
            Layout.alignment: Qt.AlignHCenter

            onTextChanged: invalidDataLabel.visible = false
            placeholderText: "Login"
        }

        TextField {
            id: passwordTextField
            Layout.fillWidth: true
            Layout.maximumWidth: SMA.scaled(600)
            Layout.alignment: Qt.AlignHCenter
            KeyNavigation.tab: logInButton
            echoMode: TextInput.Password
            onTextChanged: invalidDataLabel.visible = false
            placeholderText: "Password"
        }

        Label {
            id: invalidDataLabel
            Layout.fillWidth: true
            Layout.maximumWidth: SMA.scaled(600)
            text: "Invalid login or password"
            visible: false
        }

        Button {
            id: logInButton
            Layout.fillWidth: true
            Layout.maximumWidth: SMA.scaled(600)
            Layout.alignment: Qt.AlignHCenter
            text: "Log In"
            property string color: "#659cc9"
            KeyNavigation.tab: signUpButton
            onClicked: {
                busyIndicator.running = true
                invalidDataLabel.visible = false
                root.loginClicked(loginTextField.text, passwordTextField.text)
            }
        }

        Label {
            text: "Or"
            Layout.alignment: Qt.AlignHCenter
        }

        Button {
            id: signUpButton
            Layout.fillWidth: true
            Layout.maximumWidth: SMA.scaled(600)
            Layout.alignment: Qt.AlignHCenter
            text: "Sign Up"
            property string color: "#659cc9"
            KeyNavigation.tab: loginTextField
            onClicked: root.signUpClicked()
        }


        /*  This is a clickable text label to provide custom action when the user clicks on this component.
            This component emits passwordForgotClicked() signal whenever it's clicked.
        */
        Label {
            id: passwordForgotLabel
            Layout.fillWidth: true
            Layout.maximumWidth: SMA.scaled(600)
            Layout.alignment: Qt.AlignHCenter
            text: "<a href=\"www.example.org\"><font color=\"FF00CC\">Forgot password?</font></a>"
            onLinkActivated: root.passwordForgotClicked()
        }
    }

    /*  Component used to inform the user that something is happening
        When this component is running, entire panel is disabled
    */
    BusyIndicator {
        id: busyIndicator
        anchors.centerIn: parent
        running: false
    }
}
