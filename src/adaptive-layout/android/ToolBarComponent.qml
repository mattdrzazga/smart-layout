import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

ToolBar {
    id: root
    property Component toolBarLayout: undefined
    property string color

    style: ToolBarStyle {
        panel: Rectangle {
            color: root.color
        }
    }

    Loader {
        id: loader
        anchors.fill: parent
        sourceComponent: toolBarLayout
    }
}
