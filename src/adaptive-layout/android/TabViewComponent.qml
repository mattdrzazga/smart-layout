import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import "qrc:/core/src/core"

Rectangle {
    id: root
    property list<Tab> tabs


    Rectangle {
        id: tabRect
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: SMA.scaled(100)

        RowLayout {
            anchors.fill: parent
            spacing: 0

            Repeater {
                model: tabs

                Rectangle {
                    id: rect
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    color: "#00BCD4"

                    property bool isCurrentTab: listView.currentIndex === index

                    Label {
                        anchors.centerIn: parent
                        text: modelData.title
                        font.capitalization:  Font.AllUppercase
                        color: rect.isCurrentTab? "white" : "#B2EBF2"
                    }

                    Rectangle {
                        width: parent.width
                        anchors.bottom: parent.bottom
                        height: SMA.scaled(10)
                        color: "#1e2d00"
                        visible: index === listView.currentIndex
                    }

                    RippleEffect {
                        anchors.fill: parent
                        source: parent
                        onClicked: listView.currentIndex = index
                    }
                }
            }
        }
    }

    ListView {
        id: listView
        width: parent.width
        anchors.top: tabRect.bottom
        anchors.bottom: parent.bottom
        model: tabs
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        boundsBehavior: Flickable.StopAtBounds
        highlightMoveDuration: 100
        pixelAligned: true
        maximumFlickVelocity: 10000

        delegate: Loader {
            width: listView.width
            height: listView.height
            sourceComponent: modelData.sourceComponent
        }

        onMovingChanged: updateCurrentIndex()

        function updateCurrentIndex() {
            var candidate = 0
            for (var i = 0; i < tabs.length; ++i) {
                if (contentX >= (i * listView.width)) {
                    candidate = i
                }
            }
            currentIndex = candidate
        }
    }
}
