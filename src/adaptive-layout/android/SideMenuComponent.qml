import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import "qrc:/core/src/core"

Rectangle {
    id: root
    width: parent.width
    height: parent.height
    x: -width
    focus: true

    property bool menuVisible: false
    property list<Action> actions

    Keys.onBackPressed: root.hide()

    Behavior on x {
        PropertyAnimation {
            easing.type: Easing.InOutQuad
            duration: 200
        }
    }

    // Dimmed area
    Rectangle {
        id: dimmedRect
        height: parent.height
        width: 2 * parent.width
        anchors.left: parent.right
        color: "black"
        opacity: 0

        Behavior on opacity { PropertyAnimation { duration: 150 } }

        MouseArea {
            anchors.fill: parent
            onClicked: root.hide()
        }
    }

    function show() {
        x = 0
        dimmedRect.opacity = 0.6
        menuVisible = true
        root.forceActiveFocus()
    }

    function hide() {
        x = -width
        dimmedRect.opacity = 0
        menuVisible = false
        root.parent.forceActiveFocus()
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        Repeater {
            model: actions

            MobileMenuTile {
                Layout.preferredWidth: parent.width
                Layout.preferredHeight: SMA.scaled(120)

                text: modelData.text
                iconSource: modelData.iconSource
                checked: modelData.checked
                exclusiveGroup: modelData.exclusiveGroup

                onClicked: {root.hide(); modelData.trigger()}
            }
        }

        Item { Layout.fillHeight: true }
    }
}
