import QtQuick 2.5
import QtGraphicalEffects 1.0
import "qrc:/adaptive-layout/android/src/adaptive-layout/android" as Android
import "qrc:/adaptive-layout/desktop/src/adaptive-layout/desktop" as Desktop

Loader {
    id: root
    anchors.fill: parent

    //  This property hold the color value.
    property string color: "#f44336"

    // This property holds whether the desktop version of the ToolBar will be colored.
    property bool colorOnDesktop: false

    //  This property hold the layout that is used inside ToolBar
    property Component toolBarLayout: undefined

    //  Mobile version of the ToolBar component
    property Component androidToolBar: Android.ToolBarComponent {
        id: toolBar
        toolBarLayout: root.toolBarLayout
        color: root.color
        Component.onCompleted: dropShadow.source = toolBar
        Component.onDestruction: dropShadow.source = null
    }

    //  Desktop version of the ToolBar component
    property Component desktopToolBar: Desktop.ToolBarComponent {
        toolBarLayout: root.toolBarLayout
        color: root.color
        colorOnDesktop: root.colorOnDesktop
    }

    sourceComponent: SMA.isMobile? androidToolBar : desktopToolBar


    //  Shadow effect, visible only on mobile devices.
    DropShadow {
        id: dropShadow
        visible: SMA.isMobile
        anchors.fill: root
        verticalOffset: 3
        radius: 8.0; samples: 16
        transparentBorder: true
        color: "#80000000"
    }
}
