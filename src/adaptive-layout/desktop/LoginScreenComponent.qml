import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

Window {
    id: root

    signal loginClicked(string login, string password)
    signal signUpClicked()
    signal passwordForgotClicked()

    width: SMA.scaled(640)
    height: SMA.scaled(480)

    maximumWidth: width
    maximumHeight: height

    minimumWidth: width
    minimumHeight: height

    title: "Log In"
    flags: Qt.WindowCloseButtonHint


    Component.onCompleted: {
        rootWindow.visible = false
        show()
    }

    property string backgroundImage
    property string title
    property Window rootWindow: null


    function wrongPassword() {
        invalidDataLabel.visible = true
        busyIndicator.running = false
    }

    // http://wallpaperswide.com/coloured_pencils-wallpapers.html
    Image {
        id: image
        anchors.fill: parent
        source: backgroundImage
    }

    FastBlur {
        anchors.fill: image
        source: image
        radius: 64
    }

    Item {

        /*  This property holds login that user typed.
        */
        property alias login: loginTextField.text
        /*  This property holds password that user typed.
        */
        property alias password: passwordTextField.text

        /*  This is alias to invalidDataLabel object.
        */
        property alias invalidDataLabel: invalidDataLabel

        /*  This is alias to busyIndicator object.
        */
        property alias busyIndicator: busyIndicator

        /*  This element is disabled when busyIndicator is running.
        */
        enabled: !busyIndicator.running

        anchors.centerIn: parent
        width: SMA.scaled(250)
        height: SMA.scaled(250)


        /*  This Rectangle is used as a background.
            Setting 'color' property to black and 'opacity' to i.e 0.3 will give
            effect known from many mobile application login screens.
            It is extremely sexy for the eye when one puts nice Image on top and later uses
            FancyLoginPanel component with 'color' and 'opacity' properties set as above.
        */
        Rectangle {
            anchors.fill: parent
            color: "#b4eeb4"
            opacity: 0.4
        }

        ColumnLayout {
            id: column
            anchors.fill: parent
            anchors.margins: SMA.scaled(20)
            spacing: SMA.scaled(5)

            TextField {
                id: loginTextField
                Layout.fillWidth: true
                Layout.preferredHeight: SMA.scaled(30)
                focus: true
                KeyNavigation.tab: passwordTextField
                onTextChanged: invalidDataLabel.visible = false
                placeholderText: "Login"
            }

            TextField {
                id: passwordTextField
                Layout.fillWidth: true
                Layout.preferredHeight: SMA.scaled(30)
                KeyNavigation.tab: logInButton
                echoMode: TextInput.Password
                onTextChanged: invalidDataLabel.visible = false
                placeholderText: "Password"
            }

            /*  Text label to inform the user that he/she provided invalid data.
            */
            Label {
                id: invalidDataLabel
                Layout.fillWidth: true
                text: "Invalid login or password"
                visible: false
            }


            /*  Log In Button
            */
            Button {
                id: logInButton
                Layout.fillWidth: true
                Layout.preferredHeight: SMA.scaled(30)
                style: buttonStyle
                text: "Log In"
                property string color: "#659cc9"
                KeyNavigation.tab: signUpButton
                onClicked: {
                    busyIndicator.running = true
                    invalidDataLabel.visible = false
                    root.loginClicked(loginTextField.text, passwordTextField.text)
                }
            }

            Label {
                text: "Or"
                Layout.alignment: Qt.AlignHCenter
            }


            /*  Sign Up Button
            */
            Button {
                id: signUpButton
                Layout.fillWidth: true
                Layout.preferredHeight: SMA.scaled(30)
                style: buttonStyle
                text: "Sign Up"
                property string color: "#659cc9"
                KeyNavigation.tab: loginTextField
                onClicked: root.signUpClicked()
            }


            /*  Simple horizontal line separator
            */
            Rectangle {
                id: separator
                Layout.fillWidth: true
                Layout.preferredHeight: SMA.scaled(1)
                color: "#03396c"
            }

            /*  This is a clickable text label to provide custom action when the user clicks on this component.
                This is particularly useful when the user forgets its password and wants to perform password recovery action.
                In that case, second window with form should be openned.
                This component emits passwordForgotClicked() signal whenever it's clicked.
            */
            Label {
                id: passwordForgotLabel
                Layout.fillWidth: true
                text: "<a href=\"www.example.org\"><font color=\"FF00CC\">Forgot password?</font></a>"
                onLinkActivated: passwordForgotClicked()
                anchors.left: parent.left
            }
        }

        /*  Component used to inform the user that something is happening
            When this component is running, entire panel is disabled
        */
        BusyIndicator {
            id: busyIndicator
            anchors.centerIn: parent
            running: false
        }
    }

    Component {
        id: buttonStyle
        ButtonStyle {
            background: Item {
                property string ccolor: control.color
                property bool down: control.pressed || (control.checkable && control.checked)
                property string borderColor: "#47b"

                Rectangle {
                    id: baserect
                    color: down? Qt.darker(ccolor, 1.25) : ccolor
                    anchors.fill: parent
                    border.color: control.activeFocus ? "#47b" : "darkgrey"
                    border.width: SMA.scaled(1)
                    radius: SMA.scaled(2)

                    Rectangle {
                        anchors.fill: parent
                        radius: parent.radius
                        color: control.activeFocus ? "#47b" : Qt.lighter("white", 1.25)
                        opacity: control.hovered || control.activeFocus ? 0.1 : 0
                        Behavior on opacity { NumberAnimation { duration: 100 } }
                    }
                }
            }
        }
    }
}
