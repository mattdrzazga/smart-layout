import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import "qrc:/core/src/core"

Rectangle {
    width: parent.width
    height: parent.height
    x: -width

    property bool menuVisible: false
    property list<Action> actions


    // Right border
    Rectangle { width: 1; height: parent.height; anchors.right: parent.right; color: "#000000" }


    function show() {
        x = 0;
        menuVisible = true
    }

    function hide() {
        x = -width;
        menuVisible = false
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        Repeater {
            model: actions

            DesktopMenuTile {
                Layout.preferredWidth: parent.width - 1
                Layout.preferredHeight: SMA.scaled(70)
                iconSource: modelData.iconSource
                text: modelData.text
                action: actions[index]
            }
        }

        Item { Layout.fillHeight: true }
    }
}
