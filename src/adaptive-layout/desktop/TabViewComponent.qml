import QtQuick 2.5
import QtQuick.Controls 1.4

TabView {
    id: tabView
    property list<Tab> tabs

    Component.onCompleted: {
        for (var i = 0; i < tabs.length; ++i) {
            tabView.addTab(tabs[i].title, tabs[i].sourceComponent)
        }
    }
}
