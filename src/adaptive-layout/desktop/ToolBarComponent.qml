import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ToolBar {
    id: root
    property Component toolBarLayout: undefined
    property string color
    property bool colorOnDesktop: false

    onColorOnDesktopChanged: {
        if (colorOnDesktop)
            root.style = colorStyle

    }

    property Component colorStyle: ToolBarStyle {
        background: Rectangle {
            color: root.color
        }
    }

    Loader {
        anchors.fill: parent
        sourceComponent:  toolBarLayout
    }
}
