import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Rectangle {
    id: root
    property var model: undefined
    property Component listDelegate: undefined
    property Component detailComponent: undefined
    property string cellColor: "white"
    property string highlightColor: "#18aa42"
    property bool striped: true

    property alias listView: listView

    SplitView {
        anchors.fill: parent

        ListView {
            id: listView
            width: SMA.scaled(200)
            boundsBehavior: Flickable.StopAtBounds
            currentIndex: -1

            delegate: Rectangle {
                id: background
                width: parent.width
                color: root.striped? model.index % 2 == 0? cellColor : Qt.darker(cellColor, 1.03) : cellColor

                Rectangle {
                    anchors.fill: parent
                    color: highlightColor
                    visible: index === listView.currentIndex
                }

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: listView.currentIndex = index

                    Rectangle {
                        anchors.fill: parent
                        opacity: 0.6
                        color: Qt.lighter(highlightColor, 2)
                        visible: parent.containsMouse
                    }
                }

                Loader {
                    id: loader
                    sourceComponent: root.listDelegate

                    // THIS IS BRILLIANT; PART 2!!!
                    onLoaded: {
                        item.model = model
                        item.modelData = modelData

                        background.height = loader.item.height
                        loader.anchors.fill = background
                    }

                    Connections {
                        target: loader.item
                        onHeightChanged: {
                            loader.anchors.fill = undefined
                            background.height = loader.item.height
                            loader.anchors.fill = background
                        }
                    }
                }
            }

            model: root.model
        }

        Loader {
            sourceComponent: root.detailComponent
        }
    }
}
