import QtQuick 2.5
import QtQuick.Window 2.0
import "qrc:/adaptive-layout/android/src/adaptive-layout/android" as Android
import "qrc:/adaptive-layout/desktop/src/adaptive-layout/desktop" as Desktop

Loader {
    id: root

    //  Signal is emitted when the user clicks login button.
    signal loginClicked(string login, string password)

    //  Signal is emitted when the user clicks signUp button.
    signal signUpClicked()

    //  Signal is emitted when the user clicks forgot password label.
    signal passwordForgotClicked()

    //  This property holds form background color.
    property string backgroundColor

    //  This property holds path to the image, that is used as a background.
    property string backgroundImage

    // This property holds main window of the application. It is used to style window with login screen.
    // On desktop platforms this component is based on Window component, thus requires a reference to main window.
    property Window rootWindow: null

    //  Function used to close login screen.
    function close() {
        if (root.item) root.item.close()
    }

    //  Function used to inform login screen that given credentials are wrong.
    function wrongPassword() {
        if (root.item) root.item.wrongPassword()
    }

    property Component androidLoginScreen: Android.LoginScreenComponent {
        color: root.backgroundColor
        backgroundImage: root.backgroundImage
        onLoginClicked: root.loginClicked(login, password)
        onSignUpClicked: root.signUpClicked()
        onPasswordForgotClicked: root.passwordForgotClicked()
    }

    property Component desktopLoginScreen: Desktop.LoginScreenComponent {
        color: root.backgroundColor
        backgroundImage: root.backgroundImage
        onLoginClicked: root.loginClicked(login, password)
        onSignUpClicked: root.signUpClicked()
        onPasswordForgotClicked: root.passwordForgotClicked()
        rootWindow: root.rootWindow
    }

    sourceComponent: SMA.isMobile? androidLoginScreen : desktopLoginScreen
}
