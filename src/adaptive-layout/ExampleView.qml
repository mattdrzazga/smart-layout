import QtQuick 2.5
import QtQuick.Controls 1.4
import "qrc:/adaptive-layout/android/src/adaptive-layout/android" as Android
import "qrc:/adaptive-layout/desktop/src/adaptive-layout/desktop" as Desktop

Loader {
    id: root

    /*
        Add your properties here and remember to pass them to platform specific components.
        Example:
        property int value: 10
    */

    property Component androidExampleView: Android.ExampleViewComponent {
        //  value: root.value
    }

    property Component desktopExampleView: Desktop.ExampleViewComponent {
        //  value: root.value
    }

    sourceComponent: SMA.isMobile? androidExampleView : desktopExampleView
}
