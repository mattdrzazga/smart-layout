import QtQuick 2.5
import "qrc:/themed-layout/themes/desktop/src/themed-layout/themes/desktop" as TLD
import "qrc:/themed-layout/themes/android/src/themed-layout/themes/android" as TLA

Item {
    id: rootItem

    //  Default style is desktop style.
    property Component style: TLD.ExampleViewStyle {}

    /*
        Component logic here
    */

    // Loader for loading background
    Loader {
        id: backgroundLoader
        anchors.fill: parent
        sourceComponent: styleLoader.item.background
    }

    // Loader that is used to load style.
    Loader {
        id: styleLoader
        sourceComponent: rootItem.style
        visible: false
    }
}
