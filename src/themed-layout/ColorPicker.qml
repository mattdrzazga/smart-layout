import QtQuick 2.5
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import "qrc:/themed-layout/themes/desktop/src/themed-layout/themes/desktop" as TLD

Dialog {
    id: root
    title: "Select Color"

    //  This property contains model data.
    property var model

    //  This property holds the color value of currently selected color.
    property string currentColor: ""

    //  This signal is emmited each time the user selects new color.
    signal colorSelected(string color)

    //  This property hold desktop style for ColorPicker
    //  To use it on mobile, change TLD to TLA.
    property Component style: TLD.ColorPickerStyle {}

    onModelChanged: if (loader.item)  loader.item.model = model

    contentItem: loader.item


    Loader {
        id: loader
        visible: false
        asynchronous: true
        sourceComponent: root.style

        onLoaded: item.model = root.model


        Connections {
            target: loader.item

            onColorSelected: {
                root.currentColor = color
                colorSelected(color)
            }
        }
    }
}
