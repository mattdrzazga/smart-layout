import QtQuick 2.5
import QtQuick.Controls 1.4
import "qrc:/themed-layout/themes/desktop/src/themed-layout/themes/desktop" as TLD
import "qrc:/themed-layout/themes/android/src/themed-layout/themes/android" as TLA

Button {
    id: root

    property string color: "#e3e3e3"

    property alias mouseArea: mouseArea

    property Component theme: TLD.ButtonStyle { }
    style: theme

    MouseArea {
        id: mouseArea
        enabled: SMA.isMobile
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true
        clip: true

        onReleased: { root.clicked(); mouse.accepted = false }
    }
}
