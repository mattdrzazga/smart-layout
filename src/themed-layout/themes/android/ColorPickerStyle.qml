import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

Rectangle {
    id: root
    implicitWidth: SMA.scaled(560)
    implicitHeight: gridLayout.implicitHeight + 2 * SMA.scaled(30) + selectColorLabel.implicitHeight + SMA.scaled(30)
    clip: true

    property var model
    property string currentColor: ""
    property int currentIndex: -1

    signal colorSelected(string color)

    Label {
        id: selectColorLabel
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: SMA.scaled(30)
        font.pointSize: 22
        text: "Select color"
    }

    GridLayout {
        id: gridLayout
        height: SMA.scaled(220)
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: selectColorLabel.bottom
        anchors.bottom: parent.bottom
        anchors.leftMargin: SMA.scaled(50)
        anchors.rightMargin: SMA.scaled(50)
        anchors.topMargin: SMA.scaled(30)
        anchors.bottomMargin: SMA.scaled(30)
        rows: 2
        columns: 4
        rowSpacing: SMA.scaled(20)
        columnSpacing: SMA.scaled(20)

        Repeater {
            model: colors

            Rectangle {
                width: SMA.scaled(100)
                height: SMA.scaled(100)
                radius: SMA.scaled(50)
                color: modelData
                border.width: SMA.scaled(1)
                border.color: "lightgrey"
                antialiasing: true

                Rectangle {
                    anchors.centerIn: parent
                    width: parent.width * 1.3
                    height: parent.height * 1.3
                    color: "lightgrey"
                    radius: width / 2
                    antialiasing: true
                    z: -1

                    opacity: mouseArea.containsMouse? 1 : 0

                    Behavior on opacity { PropertyAnimation {} }
                }

                Image {
                    width: parent.width / 2
                    height: parent.height / 2
                    anchors.centerIn: parent
                    asynchronous: true
                    cache: true
                    antialiasing: true
                    source: "qrc:/icons/icons/ic_done_black_48dp.png"
                    visible: root.currentIndex === index
                }

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        root.currentColor = modelData
                        root.currentIndex = index
                        root.colorSelected(root.currentColor)
                    }
                }
            }
        }
    }
}
