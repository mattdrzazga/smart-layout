import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

ButtonStyle {
    id: root

    Label {
        id: evaluator
        visible: false
        text: control.text
    }

    background: Item {
        property bool down: control.pressed || (control.checkable && control.checked)
        implicitWidth: Math.round(evaluator.implicitHeight * 4.5)
        implicitHeight: Math.max(SMA.scaled(25), Math.round(evaluator.implicitHeight * 1.2))

        Rectangle {
            anchors.fill: parent
            anchors.bottomMargin: down ? 0 : -1
            radius: rect.radius
            color: "#10000000"
        }

        Rectangle {
            id: rect
            anchors.fill: parent
            radius: evaluator.implicitHeight * 0.16
            color: down? Qt.darker(control.color, 1.4) : control.color
            border.color: control.activeFocus ? "#47b" : "#999"

            Rectangle {
                anchors.fill: parent
                radius: parent.radius
                color: control.activeFocus ? "#47b" : "white"
                opacity: control.hovered || control.activeFocus ? 0.1 : 0
                Behavior on opacity { NumberAnimation { duration: 100 } }
            }
        }
    }

}
