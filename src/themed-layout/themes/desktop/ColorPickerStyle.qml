import QtQuick 2.5

Rectangle {
    id: root
    implicitWidth: SMA.scaled(160)
    implicitHeight: SMA.scaled(160)

    property var model
    property string currentColor: ""
    property int currentIndex: -1

    signal colorSelected(string color)

    GridView {
        anchors.fill: parent
        anchors.margins: SMA.scaled(10)
        anchors.rightMargin: SMA.scaled(0)
        model: root.model
        boundsBehavior: Flickable.StopAtBounds
        cellWidth: SMA.scaled(50)
        cellHeight: SMA.scaled(50)

        delegate: Rectangle {
            width: SMA.scaled(40)
            height: SMA.scaled(40)
            radius: SMA.scaled(20)
            color: root.model[index]

            scale: mouseArea.containsMouse? 1.2 : 1.0

            Image {
                width: parent.width / 2
                height: parent.height / 2
                anchors.centerIn: parent
                asynchronous: true
                cache: true
                source: "qrc:/icons/icons/ic_done_black_48px.svg"
                visible: root.currentIndex === index
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    root.currentColor = root.model[index]
                    root.currentIndex = index
                    root.colorSelected(root.currentColor)
                }
            }
        }
    }
}
