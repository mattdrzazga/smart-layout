import QtQuick 2.5

MouseArea {
    id: root
    anchors.fill: parent

    property Item source: null

    onPressed: {
        rippleBox.hotspotX = mouse.x
        rippleBox.hotspotY = mouse.y
        rippleAnimation.running = true
    }

    onReleased: rippleAnimation.complete()

    Item {
        id: ripple
        anchors.fill: parent
        visible: false

        Rectangle {
            id: rippleBox

            property real hotspotX
            property real hotspotY

            x: hotspotX - width / 2
            y: hotspotY - height / 2

            width: 0
            height: width

            radius: width / 2
            color: "grey"
        }

        SequentialAnimation {
            id: rippleAnimation
            ScriptAction { script: {
                    rippleBox.width = 0;
                    rippleBox.opacity = 0.3
                    ripple.visible = true;
                    root.source.layer.enabled = true;
                    ripple.layer.enabled = true;
                }
            }

            NumberAnimation { target: rippleBox; property: "width"; from: 0; to: root.source.width * 3; duration: 600 }
            NumberAnimation { target: rippleBox; property: "opacity"; to: 0; duration: 250 }
            ScriptAction { script: {
                    root.source.layer.enabled = false;
                    ripple.layer.enabled = false;
                    ripple.visible = false;
                }
            }
        }
    }
}
