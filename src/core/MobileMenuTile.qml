import QtQuick 2.5
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.3

Item {
    id: root

    signal clicked()

    property alias iconSource: iconImage.source
    property alias text: textLabel.text
    property bool checked: false
    property ExclusiveGroup exclusiveGroup: null

    onExclusiveGroupChanged: {
        if (exclusiveGroup)
            exclusiveGroup.bindCheckable(root)
    }


    // Current tile indicator
    Rectangle {
        anchors.fill: parent
        color: "#c0c0c0"
        visible: root.checked
    }

    RowLayout {
        spacing: SMA.scaled(60)
        anchors.fill: parent
        anchors.margins: SMA.scaled(30)

        Image {
            id: iconImage
            Layout.preferredHeight: SMA.scaled(48)
            Layout.preferredWidth: SMA.scaled(48)
        }

        Label {
            id: textLabel
            Layout.fillHeight: false
            Layout.fillWidth: true
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            root.checked = true
            root.clicked()
        }
    }

    RippleEffect {
        source: root
        propagateComposedEvents: true
    }
}
