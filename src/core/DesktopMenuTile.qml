import QtQuick 2.5
import QtQuick.Controls 1.4

ToolButton {
    text: "None"
    checkable: true

    Label {
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        text: parent.text
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
    }
}
